
import React, { useState } from 'react';
import Axios from 'axios';
import Navbar from '../Navbar/Navbar'
import './Products.css';
const AllProducts = () => {

  const [products, setProducts] = useState([]);
  const getProducts = () => {
    Axios.get('http://localhost:3001/products', {
      headers: {
        "Authorization": localStorage.getItem('token')
      }
    })
      .then((res) => {
        setProducts(res.data.products_data)
        console.log(res.data)
        console.log(products)
      })
      .catch((err) => {
        console.log(err)
      })

  }
  return (
    // <div className="container d-flex align-items-center flex-column">
    <div>
      <Navbar />
      <h1>Nuestros productos</h1>
      <button onClick={getProducts}>Mostrar Listado</button>
    {/* <div className="screen-container">
      <div className="products-container"> */}
        {products.map(product => (
          <div className="product-container">
            <h2 className="product-name">{product.name}</h2>
            <h3 className="product-price">{product.precio}</h3>
            <a className="product-btn" onclick="" id={product.id}>+</a>
          </div>
        ))}
      {/* </div>

    </div> */}
    </div>
  )
}

export default AllProducts