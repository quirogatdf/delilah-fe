import React, { useEffect, useState } from "react";
import Header from '../Header/Header';
import { formConfig } from './formConfig.js';
import useMercadoPago from "../../hooks/useMercadoPago.js";

const INITIAL_STATE = {
    cvc:"",
    cardExpirationMonth: "",
    cardExpirationYear: "",
    focus: "cardNumber",
    cardholderName: "",
    cardNumber: "",
    issuer: "",
};
export default function MercadoPagoForm() {
const [state, setState] = useState(INITIAL_STATE);
const resultPayment = useMercadoPago();
    
const handleInputChange = (e) => {
    setState({
    ...state,
    [e.target.dataset.name || e.target.name]: e.target.value,
    });
};

const handleInputFocus = (e) => {
    setState({ ...state, focus: e.target.dataset.name || e.target.name });
};
return (
    <div>
    <Header />
    <div className="container d-flex align-items-center flex-column">
<h1>Simulación de pago con MercadoPago</h1>
            <form id="form-checkout" className="login-card col-12 col-lg-6 hv-center">
                <div className="form-group text-left">
                    <label> Número de Tarjeta</label>
                    <input
                        type="tel"
                        name="cardNumber"
                        id="form-checkout__cardNumber"
                        className="form-control"
                        placeholder="Numero de Tarjeta"
                        onChange={handleInputChange}
                        onFocus={handleInputFocus}
                    />
                </div>
                <label>Fecha de vencimiento</label>
                <div className="row form-group">
                <div className="col-4">
                    <input
                        type="tel"
                        name="cardExpirationMonth"
                        id="form-checkout__cardExpirationMonth"
                        placeholder="MM"
                        className="form-control"
                        onChange={handleInputChange}
                        onFocus={handleInputFocus}
                    />
                </div>
                <div className="col-4">
                    <input
                        type="tel"
                        name="cardExpirationYear"
                        id="form-checkout__cardExpirationYear"
                        placeholder="AA"
                        className="form-control"
                        onChange={handleInputChange}
                        onFocus={handleInputFocus}
                    />
                </div>
                </div>
                <label>Código CVC</label>
                <div className="form-group">
                    <input
                        type="tel"
                        name="cvc"
                        id="form-checkout__securityCode"
                        className="form-control col-4"
                        onChange={handleInputChange}
                        onFocus={handleInputFocus}
                    />
                </div>
                <label>Nombre Completo</label>
                <div className="form-group">
                    <input
                        type="text"
                        name="cardholderName"
                        className="form-control"
                        id="form-checkout__cardholderName"
                        onChange={handleInputChange}
                        onFocus={handleInputFocus}
                    />
                    </div>
                    <label>E-mail</label>
                <div className="form-group">
                    <input
                        type="email"
                        name="cardholderEmail"
                        className="form-control"
                        id="form-checkout__cardholderEmail"
                        onFocus={handleInputFocus}
                    />
                </div>
                <label>Banco emisor</label>
                <div className="form-group col-md-6">
                    <select
                        className="form-select form-control "
                        name="issuer"
                         aria-label="select"
                        id="form-checkout__issuer"
                        on
                    ></select>
                </div>
                <label>Tipo de documento</label>
                <div className="form-group col-md-6">
                    <select
                        className="form-select form-control"
                        name="identificationType"
                        id="form-checkout__identificationType"
                    ></select>
                </div>
                <label>Numero de documento</label>
                <div className="form-group">
                    <input
                        type="text"
                        className="form-control"
                        name="identificationNumber"
                        id="form-checkout__identificationNumber"
                    />
                </div>
                <label>Cuotas</label>
                <div className="form-group col-4">
                    <select
                        name="installments"
                        className="form-control form-select"
                        id="form-checkout__installments"
                    ></select>
                </div>
                <br/>
                <div className="d-grid gap-2 col-6 mx-auto">
                    <button type="submit"
                            id="form-checkout__submit"
                            className="btn btn-primary btn-lg">
                        Pagar
                    </button>
                </div>
                <div className="progress">
                <progress value="0" id="progress-bar" className="progress-bar progress-bar-success">
                    Cargando...
                </progress>
				</div>
            </form>
            {resultPayment && <p>{JSON.stringify(resultPayment)}</p>}
</div>
    </div>
)
};
