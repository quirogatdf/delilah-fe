import React from 'react';
import Axios from 'axios'
import { useState } from 'react';
import './LoginForm.css';
import Header from '../Header/Header'
import { useNavigate } from 'react-router-dom'
//const history = useHistory();
//import { API_BASE_UR } from '../../constants/apiConstants';
const LoginForm = () => {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loginStatus, setLoginStatus] = useState("");
  //const [token, setToken] = useState("");

  const login = () => {
    Axios.post('https://www.delilah-resto.tk/api/login',{username: username, password: password},{headers:{'Access-Control-Allow-Origin': '*'}})
    .then((response) => {
      if(response.data.auth){
          setLoginStatus(response.data.message)
          localStorage.setItem('token', `Bearer ${response.data.token}`)
          console.log(localStorage.getItem('token'))
        } else {
          setLoginStatus(response.data.message)
        }

        //setLoginStatus(response.data.auth);
        //console.log(response)
        //setToken(response.data.token)
        //Limpiar variables
        setUsername("");
        setPassword("");
      })
      .catch((err) => {
        console.log(err);
        setLoginStatus(err.data)
      })
  };

  return (
    <div>
      <Header />

      <div className="container d-flex align-items-center flex-column">
        <div className="col-12 col-lg-4 login-card mt-2 hv-center">
          {/* Input de username */}
          <div className="form-group text-left" >
            <label htmlFor="exampleInputEmail1">Usuario o correo electrónico</label>
            <input type="text"
              className="form-control"
              id="username"
              placeholder="Ingresa con tu usuario registrado"
              value={username}
              onChange={(event) => {
                setUsername(event.target.value);
              }}
            />
            {/* <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small> */}
          </div>
          {/* Input de contraseña */}
          <div className="form-group text-left">
            <label htmlFor="exampleInputPassword1">Contraseña</label>
            <input type="password"
              className="form-control"
              id="password"
              placeholder="Ingresa la contraseña"
              autoComplete="new-password"
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
          </div>
          {/* Boto de logue */}
          <button
            type="submit"
            className="btn-delilah"
            onClick={login}
          >INICIAR SESIÓN</button>
          <h2>{loginStatus}</h2>
	  <label htmlFor="exampleInputPassword1" >or login with:</label><br/>
	  <div className="providers">
	    <button className="btn-external-login GoogleBtn" onClick={(e) => {
  	      e.preventDefault();
	      window.location.href='https://www.delilah-resto.tk/api/google/auth';
	    }}data-provider="google">Google</button>
	    <button className="btn-external-login LinkedInBtn" onClick={(e) => {
          e.preventDefault();
          window.location.href='https://www.delilah-resto.tk/api/linkedin/auth';
        }}>LinkedIn</button>
        <button className="btn-external-login Auth0" onClick={(e)=> {
          e.preventDefault();
         window.location.href='/api/auth0/login';
        }}>Auth0</button>

        <button className="btn-external-login MercadopagoBtn" onClick={(e)=> {
        navigate('/mercadopago')
        }}>Simulate payment with Mercadopago</button>
	  </div>
        </div>
      </div>
    </div>
  )
};
export default LoginForm
