import './App.css';
//import './App-personalizado.css';
import Header from './components/Header/Header';
import Axios from 'axios'
import { useState } from 'react';
import {BrowserRouter, Routes, Route, Link} from "react-router-dom"
import LoginForm from './components/LoginForm/LoginForm'
import AllProducts from './components/Products/allProducts';
import MercadoPagoForm from './components/MercadoPago/MercadoPago';
function App() {

  return (
    <div className="App">    
      <BrowserRouter>
      <Routes>    
        <Route index path="/" element={<LoginForm />} />
        <Route path="home"/>
        <Route path="dashboard_admin" element={<AllProducts />} />
        <Route path="mercadopago" element={<MercadoPagoForm />}/>
      </Routes>
      </BrowserRouter>
  </div>
  );
}

export default App;
